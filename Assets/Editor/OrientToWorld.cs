﻿using UnityEngine;
using UnityEditor;

public class OrientToWorld : MonoBehaviour {

	[MenuItem("Custom/OrientToWorld")]
	static void OrentToWorld() {
		var gameObject = Selection.activeGameObject;

		if(gameObject == null) { return; }

		DebugPanel.Log("gameObject", "OrientToWorld", gameObject);

		var transform = gameObject.transform;

		var position = transform.position;
		var up = position / position.magnitude;

		DebugPanel.Log("oldRotation", "OrientToWorld", transform.rotation);
		DebugPanel.Log("up", "OrientToWorld", up);

		transform.rotation = Quaternion.LookRotation(up);
		// transform.Rotate(90.0f, 0, 0);
	}
}
