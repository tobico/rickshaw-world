﻿using UnityEngine;

public class PickupZoneController : MonoBehaviour {

	public string Name;
	public GameObject Preview;
	public GameObject CustomerPrefab;
	public GameObject DropoffPrefab;

	public void SpawnCustomer() {
		Spawn(CustomerPrefab);
	}

	public void SpawnDropoff() {
		Spawn(DropoffPrefab);
	}

	void Start() {
		Preview.SetActive(false);
	}
	
	private void Spawn(GameObject prefab) {
		Instantiate(prefab, transform.position, transform.rotation);
	}
}
