﻿using UnityEngine;

public class DropoffController : MonoBehaviour {
	public GameObject Body;
	public GameObject DropoffPoint;

	private int expire = -1;

	void OnTriggerEnter(Collider other) {
		DebugPanel.Log("trigger", "dropoff", other);

		var questController = other.GetComponent<QuestController>();
		if (questController == null) return;

		questController.Dropoff();
		Body.SetActive(true);
		GetComponent<MeshRenderer>().enabled = false;
		expire = 1000;
	}

	void Update() {
		if (expire > 0) {
			expire--;
		}
		if (expire == 0) {
			Destroy(DropoffPoint);
		}
	}
}
