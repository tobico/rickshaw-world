﻿using UnityEngine;

public class PickupController : MonoBehaviour {
	public GameObject Customer;

	void OnTriggerEnter(Collider other) {
		DebugPanel.Log("trigger", "pickup", other);

		var questController = other.GetComponent<QuestController>();
		if (questController == null) return;

		questController.Pickup();
		Destroy(Customer);
	}
}
