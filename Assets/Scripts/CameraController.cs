﻿using UnityEngine;

public class CameraController : MonoBehaviour {
	public GameObject Player;
	public float TransformLerp = 0.2f;
	public float RotateSlerp = 0.2f;

	void Update () {

		var position = Player.transform.position;
		var up = position / position.magnitude;

		var forward = Vector3.Cross(Vector3.Cross(transform.forward, up), up) * -1.0f;

		DebugPanel.Log("up", "camera", up);
		DebugPanel.Log("forward", "camera", forward);

		transform.position = Vector3.Lerp(transform.position, position, TransformLerp);
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(forward, up), RotateSlerp);
	}
}
