﻿using UnityEngine;

public class CompassController : MonoBehaviour {
	private Vector3 destination;

	void Start () {
		GetComponent<MeshRenderer>().enabled = false;
	}
	
	void Update () {
		if (destination != null) UpdateRotation();
	}

	public void SetDestination(Vector3 dest) {
		destination = dest;
		UpdateRotation();
		GetComponent<MeshRenderer>().enabled = true;
	}

	public void ClearDestination() {
		GetComponent<MeshRenderer>().enabled = false;
	}

	private void UpdateRotation() {
		var up = transform.position / transform.position.magnitude;
		var destDirection = destination - transform.position;
		destDirection /= destDirection.magnitude;

		var forward = Vector3.Cross(Vector3.Cross(destDirection, up), up);
		transform.rotation = Quaternion.LookRotation(up, forward * -1.0f);
	}
}
