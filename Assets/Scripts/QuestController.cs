﻿using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

enum State {Title, IntroAnim, Seeking, Mission, Reward, GameOver}

[System.Serializable]
public class QuestsData {
	public QuestData[] quests;
}

[System.Serializable]
public class QuestData {
	public string start;
	public string end;
	public string script;
	public int reward;
}

public class QuestController : MonoBehaviour {
	public Camera TitleCamera;
	public Camera MainCamera;
	public GameObject SittingCustomer;
	public GameObject Title;
	public GameObject HUD;
	public AudioSource TitleSound;
	public AudioSource[] GreetingSounds;
	public AudioSource[] ThanksSounds;
	public AudioSource[] ThemeSounds;
	public AudioSource MoneySound;
	public AudioSource GameoverSound;
	public Text MenuText;
	public GameObject Blanker;
	public DialogueRunner Dialogue;
	public GameObject DialogueContainer;
	public TextAsset questText;
	public CompassController Compass;
	public Text MoneyText;
	public Text MoneyEarnedText;

	public float IntroAnimLerp = 0.01f;

	public float EmptySpeed = 0.05f;
	public float FullSpeed = 0.032f;

	private int MoneySpeed = 1;

	private int t = 0;
	public int currentQuestIndex = 0;
	private State state = State.Title;
	private int money = 124;
	private int moneyEarned = 0;

	private bool music = true;
	private bool sound = true;

	private QuestData[] quests;

	private QuestData CurrentQuest {
		get {
			return quests[currentQuestIndex];
		}
	}

	void Start () {
		MainCamera.enabled = false;
		TitleCamera.enabled = true;
		Title.SetActive(false);
		HUD.SetActive(false);
		MoneyEarnedText.enabled = false;
		TitleSound.Play();
		LoadQuests();
		UpdateMoney();
	}
	
	void Update () {
		switch (state) {
			case State.Title:
				t += 1;
				Blanker.GetComponent<CanvasGroup>().alpha = 1.0f - Mathf.Min(t / 150.0f, 1.0f);
				if (t == 180) {
					UpdateMenuText();
					Title.SetActive(true);
				}
				if (Input.GetButtonDown("1")) {
					PlayIntroAnim();
					if (t < 180) {
						StartGame();
					}
				}
				if (Input.GetButtonDown("2")) {
					music = !music;
					UpdateMenuText();
				}
				if (Input.GetButtonDown("3")) {
					sound = !sound;
					UpdateMenuText();
				}
				break;
			case State.IntroAnim:
				TitleCamera.transform.position = Vector3.Lerp(TitleCamera.transform.position, MainCamera.transform.position, IntroAnimLerp);
				TitleCamera.transform.rotation = Quaternion.Slerp(TitleCamera.transform.rotation, MainCamera.transform.rotation, IntroAnimLerp);
				IntroAnimLerp *= (1.0f + IntroAnimLerp);
				if (IntroAnimLerp > 0.9f) {
					StartGame();
				}
				break;
			case State.Mission:
				t++;
				if (t == 150) {
					StartMission();
				}
				break;
			case State.Reward:
				t++;
				if (t == 150) {
					if (sound) MoneySound.Play();
				} else if (t >= 150 && t < 200) {
					if (moneyEarned > 0) {
						if (moneyEarned < MoneySpeed) {
							money += moneyEarned;
							moneyEarned = 0;
						} else {
							moneyEarned-= MoneySpeed;
							money+= MoneySpeed;
						}
						MoneySpeed++;
						t--;
						UpdateMoney();
						if (moneyEarned == 0) {
							if (sound) MoneySound.Stop();
						}
					}
				} else if (t >= 200) {
					currentQuestIndex++;
					StartQuest();
				}
				break;
			case State.GameOver:
				if (t <= 150.0f) {
					Blanker.GetComponent<CanvasGroup>().alpha = Mathf.Min(t / 150.0f, 1.0f);
					t++;
				} else {
					Application.Quit();
				}
				break;
		}
		DebugPanel.Log("state", "quest", state);
		if (Input.GetKeyDown(KeyCode.Escape)) {
    	    Application.Quit();
    	}
	}

	void PlayIntroAnim() {
		Title.SetActive(false);
		Blanker.SetActive(false);
		state = State.IntroAnim;
		TitleSound.Stop();
		PlayTheme(0);
	}

	void SwitchCameras() {
		MainCamera.enabled = true;
		TitleCamera.enabled = false;
	}

	void StartGame() {
		SwitchCameras();		
		GetComponent<PlayerController>().Disabled = false;
		GetComponent<PlayerController>().Speed = EmptySpeed;
		HUD.SetActive(true);
		StartQuest();
		//Dialogue.StartDialogue("BusinessPerson");
	}

	void UpdateMenuText() {
		var musicText = music ? "On" : "Off";
		var soundText = sound ? "On" : "Off";
		MenuText.text = string.Format("1. Play Game\n2. Music: {0}\n3. Sounds: {1}", musicText, soundText);
	}

	void PlayGreeting() {
		if (!sound) return;

		int index = Random.Range(0, GreetingSounds.Length);
		GreetingSounds[index].Play();
	}

	void PlayThanks() {
		if (!sound) return;

		int index = Random.Range(0, ThanksSounds.Length);
		ThanksSounds[index].Play();
	}

	void PlayTheme(int index) {
		foreach(AudioSource theme in ThemeSounds) {
			if (index >= 0 && theme == ThemeSounds[index]) {
				if (music) theme.Play();
			} else {
				theme.Stop();
			}
		}
	}

	private PickupZoneController FindPickupZone(string name) {
		foreach(GameObject zone in GameObject.FindGameObjectsWithTag("PickupZone")) {
			PickupZoneController controller = zone.GetComponent<PickupZoneController>();
			if (controller.Name.Equals(name)) return controller;
		}

		throw new System.ArgumentException(string.Format("can't find pickup zone with name {0}", name));
	}

	public void StartQuest() {
		DebugPanel.Log("start", "quest", currentQuestIndex);
		state = State.Seeking;
		PickupZoneController pickupZone = FindPickupZone(CurrentQuest.start);
		pickupZone.SpawnCustomer();
		DebugPanel.Log("spawnCustomer", "quest", pickupZone);
		Compass.SetDestination(pickupZone.transform.position);
	}

	public void Pickup() {
		if (state != State.Seeking) return;

		state = State.Mission;
		t = 0;
		SittingCustomer.SetActive(true);
		GetComponent<PlayerController>().Speed = FullSpeed;
		Compass.ClearDestination();
		PlayGreeting();
	}

	private void StartMission() {
		PickupZoneController pickupZone = FindPickupZone(CurrentQuest.end);

		// Don't spawn dropoff for last quest to ensure game ends
		if (currentQuestIndex != quests.Length - 1) pickupZone.SpawnDropoff();

		Compass.SetDestination(pickupZone.transform.position);
		Dialogue.StartDialogue(CurrentQuest.script);
	}

	public void Dropoff() {
		if (state != State.Mission) return;

		Dialogue.Stop();
		DialogueContainer.SetActive(false);
		SittingCustomer.SetActive(false);
		GetComponent<PlayerController>().Speed = EmptySpeed;
		t = 0;
		MoneySpeed = 1;
		moneyEarned = CurrentQuest.reward;
		state = State.Reward;
		Compass.ClearDestination();
		UpdateMoney();
		PlayThanks();
	}

	private void LoadQuests() {
		try {
			QuestsData loadedData = JsonUtility.FromJson<QuestsData>(questText.text);
			DebugPanel.Log("loadedQuests", "quest", loadedData.quests.Length);
			quests = loadedData.quests;
		} catch (System.Exception) {
			throw new System.Exception("Failed to load quests JSON");
		}
    }

	private void UpdateMoney() {
		if (moneyEarned > 0) {
			MoneyEarnedText.enabled = true;
			MoneyEarnedText.text = string.Format("+ ${0}", moneyEarned);
		} else {
			MoneyEarnedText.enabled = false;
		}
		MoneyText.text = string.Format("${0}", money);
	}

	[YarnCommand("UpendGame")]
	public void UpendGame() {
		if (music) PlayTheme(-1);
		GetComponent<PlayerController>().Disabled = true;
		if (sound) GameoverSound.Play();
	}

	[YarnCommand("EndGame")]
	public void EndGame() {
		t = 0;
		Blanker.SetActive(true);
		state = State.GameOver;
	}
}
