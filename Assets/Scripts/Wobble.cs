﻿using UnityEngine;

public class Wobble : MonoBehaviour {

	private float t = 0;
	private Vector3 scale;

	// Use this for initialization
	void Start () {
		scale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		t += 1;
		transform.rotation = Quaternion.Euler(0, 0, Mathf.Sin(t / 8.0f) * 0.6f);
		transform.localScale = scale * (1.0f + Mathf.Sin(t / 15.0f) * 0.05f);
	}
}
