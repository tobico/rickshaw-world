﻿using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float Speed = 0.03f;
	public float Gravity = 5.0f;
	public float GravityLerp = 0.2f;
	public float RotateSlerp = 0.2f;
	public GameObject MovementBase;
	public Animator ModelAnimator;
	public bool Disabled = false;

	void Update () {
		var x = Input.GetAxis("Horizontal") * Speed;
        var y = Input.GetAxis("Vertical") * Speed;

		if (Disabled) {
			x = 0;
			y = 0;
		}

		var movement = MovementBase.transform.right * x + MovementBase.transform.forward * y;
		var moving = movement.magnitude >= 0.0001f;

		ModelAnimator.SetBool("Moving", moving);

		if (!moving) return;
        transform.Translate(movement, Space.World);

		var position = transform.position;
		var up = position / position.magnitude;
		var forward = movement / movement.magnitude;

		DebugPanel.Log("up", "player", up);
		DebugPanel.Log("forward", "player", forward);

		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(forward, up), RotateSlerp);
		Physics.gravity = Vector3.Lerp(Physics.gravity, up * -1.0f * Gravity, GravityLerp);
	}
}
