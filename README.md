# Rickshaw World

## Description

[Play now on Itch](https://tobico.itch.io/rickshaw-world)

Drive a rickshaw around the tiny, colorful planet of _Rickshaw World_. There's a bunch of different customers with their own unique stories to tell, in this cross between a walking simulator and visual novel.

Control the game with your arrow keys, and select from menus using the numbers 1, 2, and 3. Best played full-screen.

## Development

Project should open and build fine in Unity 5.

## Tools used

### Software

* [Unity 5](https://unity3d.com/)
* [Blender](https://www.blender.org/)
* [GIMP](https://www.gimp.org/)
* [Inkscape](https://inkscape.org/en/)
* [Yarn](https://github.com/infiniteammoinc/Yarn)
* [Visual Studio Code](https://code.visualstudio.com/)
* [NeoTextureEdit](http://neotextureedit.sourceforge.net/)
* [Audacity](http://www.audacityteam.org/)

### Hardware

* [Blue Snowball](http://www.bluemic.com/products/snowball/)
* [Aoyama Harp](http://www.aoyama-harp.co.jp/)